<p align="center">
 <h1 align="center">LHMTR New Blocks</h1>
</p>

<p align="center">
  <a href="README_ZH-hk.md">繁体中文</a>
  ·
  <b>English(This)</b>
  ·
  <a href="README_ZH.md">简体中文</a>
</p>
This is LHMTR's Transit Expandsion mod for Minecraft Transit Railway Mod

# Introduction

You can view [Wiki](https://github.com/LHMTR/New-Blocks/wiki)

# Download

Download from the next column or download from [Modrinth](https://modrinth.com/mod/lhmtr-lhte)or Curseforge

If you see our module elsewhere, please contact XPGD_LHMTR@outlook.com immediately and report it, because this is not the code we uploaded.

# Contribution

You can check the [Contributor Guide](https://github.com/LHMTR/New-Blocks/blob/main/docs/CONTRIBUTING.md) or make:

## Fork our code and propose changes

When creating PR, please select the latest version to merge.

## Join us

For details, please refer to the organization page or contact XPGD_LHMTR@outlook.com.
