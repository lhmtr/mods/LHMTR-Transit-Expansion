# 简体中文
## 贡献者指南
欢迎阅读我们的贡献者指南！
### 贡献
我们的贡献有很多

- 更改我们的代码
- 添加新功能
- 翻译docs文件夹中的MarkDown文件
- 修改Bug

但是，请先阅读[条款与条件](https://github.com/LHMTR/LHMTR-Transit-Expansion/docs/CODE_OF_CONDUCT.md)再做出提交！
###  贡献者名单
----
#### 组织成员
##### @YYLWZDXPGD
核心成员，做出了许多贡献
----
##### @swcdjzc
做出了一些贡献
----
#### 非组织成员，在此鸣谢
##### BlockBench（APP）
一款建模软件，链接：https://blockbench.net