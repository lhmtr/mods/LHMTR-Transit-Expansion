<p align="center">
 <h1 align="center">綠海地鐵鐵道擴展</h1>
</p>

<p align="center">
  <a href="README_ZH.md">简体中文</a>
  ·
  <b>繁体中文（这个）</b>
  ·
  <a href="/README.md">English</a>
</p>
這是綠海地鐵的鐵路擴展

# 介紹
可以查看[Wiki](https://github.com/LHMTR/New-Blocks/wiki)
# 下載
從旁邊的欄目下載或者從CurseForge,[Modrinth](https://modrinth.com/mod/lhmtr-lhte)下載

如果您在其它地方看到了我們的模組，請馬上聯繫XPGD_LHMTR@outlook.com並舉報，因為這不是我們上傳的代碼
# 貢獻
可以查看[貢獻者指南](https://github.com/LHMTR/New-Blocks/blob/main/docs/CONTRIBUTING.md)或者做出：
## Fork我們的代碼並提出更改
創建PR時，請選擇最新版本來進行合併
## 加入我們
詳細可以看組織頁面或者聯繫XPGD_LHMTR@outlook.com
