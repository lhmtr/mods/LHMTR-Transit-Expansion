<p align="center">
 <h1 align="center">绿海地铁新方块</h1>
</p>

<p align="center">
  <a href="README_ZH-hk.md">繁体中文</a>
  ·
  <b>简体中文（这个）</b>
  ·
  <a href="README.md">English</a>
</p>
这是绿海地铁的铁路扩展。

# 介绍
可以查看[Wiki](https://github.com/LHMTR/New-Blocks/wiki)
# 下载
从旁边的栏目下载或者从CurseForge,[Modrinth](https://modrinth.com/mod/lhmtr-lhte)下载

如果您在其它地方看到了我们的模组，请马上联系XPGD_LHMTR@outlook.com并举报，因为这不是我们上传的代码
# 贡献
可以查看[贡献者指南](https://github.com/LHMTR/New-Blocks/blob/main/docs/CONTRIBUTING.md)或者做出：
## Fork我们的代码并提出更改
创建PR时，请选择最新版本来进行合并
## 加入我们
详细可以看组织页面或者联系XPGD_LHMTR@outlook.com
