# 简体中文
欢迎阅读我们的Github编码契约
## 条款与条件
本条件适用于所有人，包括会员

您在我们的存储库提交issue、pr或者代码，代表您同意我们的条款与条件

本存储库受到MLT License的法律保护

我们有权利更改、关闭和删除您提出的issue和pr

在政治立场中，我们支持中国，但是我们并不反对其它国家，我们欢迎所有国家的人提交！

您无权提供无关我们项目的信息
# English
Welcome to our Github configuration guide.

## Terms and Conditions

This condition applies to everyone, including members.

By submitting an issue, pr or code in our repository, you agree to our terms and conditions.

This repository is legally protected by the MLT License.

We have the right to change, close and delete your issue and pr.

In the political position, we support China, but we are not against other countries. We welcome people from all countries to submit it!

You have no right to provide information unrelated to our project.
